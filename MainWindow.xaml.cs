﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;

namespace Gestion_Stagiaire
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Champs
        private SqlConnection connexionbd; //variable pour la connexion a la bd
        DateTime d;
        #endregion


        public MainWindow()
        {
            InitializeComponent();

            //Configuration de la connexion
            connexionbd = new SqlConnection(@"Data Source=AHMED\SQLEXPRESS;Initial Catalog=Stagiaire;Integrated Security=True");

            //Chargement du combobox programme
            AjouterCbProg();
            //Chargement du combobox consulter
            AjouterCbProgConsulter();

        }

        /// <summary>
        /// Ajouter un programme a la base de donnée.
        /// La fonction verifie s'il existe un programme avec le meme nom 
        /// ou le meme numero avant de l'ajouter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Item1btn1_Click(object sender, RoutedEventArgs e)
        {
            //Variables
            programme p = new programme();//Déclaration de la variable programme.
            string requete = "insert into programme values (@numero,@nom,@duree)"; //Ma requete pour l'insertion d'un programme.
            string marequete = $"select count(*) from programme where numero = '{Item1TextBox1.Text}'  or nom = '{Item1TextBox2.Text}'";
            int count = 0;
            //Permet de passer la requete a la bd
            SqlCommand cmd = new SqlCommand(requete, connexionbd);

            //Verification des parametres
            try
            {

                p.NumeroCours = int.Parse(Item1TextBox1.Text);

            }
            catch (Exception ex)
            {
                MessageBox.Show("SVP entrez le numero de programme en bon format", "Exception Sample", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            if (!string.IsNullOrWhiteSpace(Item1TextBox2.Text))
            {
                if (!Regex.IsMatch(Item1TextBox2.Text, "^[a-zA-Z ]*$"))
                {
                    MessageBox.Show("le nom de programme doit etre en lettre seulement");
                    
                    return;
                }
                else
                {
                    p.Nom = Item1TextBox2.Text;
                }
            }
            else
            {
                MessageBox.Show("SVP entrez le nom de programme en bon format", "Exception Sample", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            try
            {
                p.Duree = int.Parse(Item1TextBox3.Text);

            }
            catch (Exception ex)
            {
                MessageBox.Show("SVP entrez la duree en bon format", "Exception Sample", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }



            //Récuperation des infromation a mettre dans les paramètres
            cmd.Parameters.AddWithValue("@numero", p.NumeroCours);
            cmd.Parameters.AddWithValue("@nom", p.Nom);
            cmd.Parameters.AddWithValue("@duree", p.Duree);
            try
            {
                
                    //La commande a éxécuter
                    SqlCommand cmd1 = new SqlCommand(marequete, connexionbd);

                    connexionbd.Open(); //Ouvrir la connexion
                    count = (int)cmd1.ExecuteScalar();
                if (count != 0)
                {
                    connexionbd.Close();
                    MessageBox.Show("Ce programme existe deja", "Verfier les informations");
                }
                else
                {
         
                    //Execution de la requête
                    cmd.ExecuteNonQuery();

                    //Femreture de la connexion
                    connexionbd.Close();

                    MessageBox.Show("Ajout avec succès!", "Confirmation de l'ajout d'un program");
                }
            }catch (Exception ex)
            {
                connexionbd.Close();
                MessageBox.Show("probleme avac la base donnee", ex.Message, MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            //Vider les champs
            Item1TextBox1.Clear();
            Item1TextBox2.Clear();
            Item1TextBox3.Clear();


        }
        /// <summary>
        /// pour charger le combobox dans l'interface consulter 
        /// </summary>
        private void AjouterCbProgConsulter()
        {
            //Requete pour récupérer les numero a partire de la table program
            string marequete = "select nom from programme";

            //La commande a éxécuter
            SqlCommand cmd = new SqlCommand(marequete, connexionbd);

            connexionbd.Open(); //Ouvrir la connexion

            SqlDataReader dr = cmd.ExecuteReader();//Lire les donnée dans la table de program

            //Chargement du Combobox avec les données de la BD
            while (dr.Read())
            {
                cb_prog_num.Items.Add(dr[0]);
            }

            connexionbd.Close(); //Fermer la connexion
        }
        /// <summary>
        /// pour charger le combobox dans l'interface stagiaire 
        /// </summary>
        private void AjouterCbProg()
        {
            //Requete pour récupérer les numero a partire de la table program
            string marequete = "select nom from programme";

            //La commande a éxécuter
            SqlCommand cmd = new SqlCommand(marequete, connexionbd);

            connexionbd.Open(); //Ouvrir la connexion

            SqlDataReader dr = cmd.ExecuteReader();//Lire les donnée dans la table de program

            //Chargement du Combobox avec les données de la BD
            while (dr.Read())
            {
                progComboBox.Items.Add(dr[0]);
            }

            connexionbd.Close(); //Fermer la connexion
        }

        /// <summary>
        /// Supprimer le programme selon le numero de programme entrée
        /// Verifie d'abord s'il existe un programme avec le meme numero saisie
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Item1btn2_Click(object sender, RoutedEventArgs e)
        {
            string marequete = "select count(*) from programme where numero = " + Item1TextBox1.Text;
            int count = 0;
            string requete = "delete from programme where numero = " + Item1TextBox1.Text;
            try
            {
                //La commande a éxécuter
                SqlCommand cmd1 = new SqlCommand(marequete, connexionbd);

                connexionbd.Open(); //Ouvrir la connexion
                count = (int)cmd1.ExecuteScalar();
                if (count == 0)
                {
                    connexionbd.Close();
                    MessageBox.Show("Ce programme n'existe pas", "Programme invalide");
                }
                else
                {
                    //Les variables

                    programme p = new programme();

                    //Permet de passer la requete a la bd
                    SqlCommand cmd = new SqlCommand(requete, connexionbd);

                    cmd.ExecuteNonQuery();

                    //Femreture de la connexion
                    connexionbd.Close();

                    //Message de confirmation
                    MessageBox.Show("Le program a été supprimé avec succès!", "Confrimation de la supression");
                }

                //Vider les siasies dans les textboxs
                Item1TextBox1.Clear();
                Item1TextBox2.Clear();
                Item1TextBox3.Clear();
            }
            catch (Exception ex)
            {
                connexionbd.Close();
                MessageBox.Show( ex.Message,"probleme avac la base donnee", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }

        /// <summary>
        /// Effacer les données entrée.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Item1btn3_Click(object sender, RoutedEventArgs e)
        {
            Item1TextBox1.Clear();
            Item1TextBox2.Clear();
            Item1TextBox3.Clear();

        }
        /// <summary>
        /// Calcul l'age du stagiaire
        /// </summary>
        /// <param name="anniversaire"> la date d'anniversaire du stagiaire</param>
       ///<returns>la fonction retourne l'age </returns>
        public int CalculAge(DateTime anniversaire)
        {
            DateTime now = DateTime.Today;
            int age = now.Year - anniversaire.Year;
            if (anniversaire > now.AddYears(-age))
                age--;
            return age;
        }

        /// <summary>
        /// Ajouter un stagiaire a la base de donnée.
        /// verfie s'il y'a numero existant du stagiaire ajoute 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Item2btn1_Click(object sender, RoutedEventArgs e)
        {

            //Variables
            Stagiaire s = new Stagiaire(); // variable pour la classe stagiaire
            string requete = "insert into stagiaire values (@numero,@nom,@prenom,@anniversaire,@sexe,@programme,@age)"; //Ma requete pour l'insertion d'un programme.
            string marequete = $"select count(*) from stagiaire where numero = '{Item2Numero.Text}'";
            int count = 0;
            //Permet de passer la requete a la bd
            SqlCommand cmd = new SqlCommand(requete, connexionbd);

            try
            {
                s.Numero = int.Parse(Item2Numero.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("SVP entrez le numero de stagiaire en bon format", "Exception Sample", MessageBoxButton.OK, MessageBoxImage.Warning);

            }

            if (!String.IsNullOrWhiteSpace(Item2Nom.Text))
            {
                if (!Regex.IsMatch(Item2Nom.Text, "^[a-zA-Z ]*$"))
                {
                    MessageBox.Show("le nom de stagiaire doit etre en lettres seulement");

                    return;
                }
                else
                {
                    s.Nom = Item2Nom.Text;
                }
                 
            }
            else
            {
                MessageBox.Show("SVP entrez le nom de stagiaire en bon format", "Exception Sample", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }


            if (!String.IsNullOrWhiteSpace(Item2Prenom.Text))
            {
                if (!Regex.IsMatch(Item2Prenom.Text, "^[a-zA-Z ]*$"))
                {
                    MessageBox.Show("le prenom doit etre en lettres");

                    return;
                }
                else
                {
                    s.Prenom = Item2Prenom.Text;
                }
            }
            else
            {
                MessageBox.Show("SVP entrez le prenom de stagiaire en bon format", "Exception Sample", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            try
            {
                d = Item2Birthdate.SelectedDate.Value;
                s.birthday = d;
            }
            catch (Exception ex)
            {
                MessageBox.Show("SVP entrez la date de naissance de stagiaire en bon format", "Exception Sample", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            if (Gender.SelectedIndex > -1)
            {

                s.sexValue = Gender.Text;
            }
            else
            {
                MessageBox.Show("SVP selectionnez une option ", "Exception Sample", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            if (progComboBox.Items.Count == 0)
            {
                MessageBox.Show("SVP remplissez la liste des programmes ", "Exception Sample", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            else
            {
                if (progComboBox.SelectedIndex > -1)
                {

                    s.program = progComboBox.Text;
                }
                else
                {
                    MessageBox.Show("SVP selectionnez un programme ", "Exception Sample", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

            }
            s.age = CalculAge(s.birthday);

            //Récuperation des infromation a mettre dans les paramètres
            cmd.Parameters.AddWithValue("@numero", s.Numero);
            cmd.Parameters.AddWithValue("@nom", s.Nom);
            cmd.Parameters.AddWithValue("@prenom", s.Prenom);
            cmd.Parameters.AddWithValue("@anniversaire", s.birthday);
            cmd.Parameters.AddWithValue("@sexe", s.sexValue);
            cmd.Parameters.AddWithValue("@programme", s.program);
            cmd.Parameters.AddWithValue("@age", s.age);
            //Ouverture de la connexion
            try {
                SqlCommand cmd1 = new SqlCommand(marequete, connexionbd);

                connexionbd.Open(); //Ouvrir la connexion
                count = (int)cmd1.ExecuteScalar();
                if (count != 0)
                {
                    connexionbd.Close();
                    MessageBox.Show("Ce stagiaire n'existe pas", "stagiaire invalide");
                }
                else
                {
                    //connexionbd.Open();

                    //Execution de la requête
                    cmd.ExecuteNonQuery();

                    //Femreture de la connexion
                    connexionbd.Close();

                    MessageBox.Show("Ajout avec succès!", "Confirmation de l'ajout d'un stagiaire");
                }
            }catch (Exception ex)
            {
                connexionbd.Close();
                MessageBox.Show("probleme avac la base donnee", ex.Message, MessageBoxButton.OK, MessageBoxImage.Warning);
            }
    //Vider les champs
            Item2Numero.Clear();
            Item2Nom.Clear();
            Item2Prenom.Clear();
            Item2Birthdate.Text = null;
            Gender.SelectedIndex = -1;
            progComboBox.SelectedIndex = -1;


        }

        /// <summary>
        /// Supprimer un stagiaire de la base de donnée
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Item2btn2_Click(object sender, RoutedEventArgs e)
        {
            //Les variables
            string requete = "delete from stagiaire where numero = " + Item2Numero.Text;
            string marequete = "select count(*) from stagiaire where numero = " + Item2Numero.Text;
            int count = 0;

            try
            {
                //La commande a éxécuter
                SqlCommand cmd1 = new SqlCommand(marequete, connexionbd);

                connexionbd.Open(); //Ouvrir la connexion
                count = (int)cmd1.ExecuteScalar();
                if (count == 0)
                {
                    connexionbd.Close();
                    MessageBox.Show("Ce stagiaire n'existe pas", "Stagiaire invalide");
                }
                else
                {
                    //Permet de passer la requete a la bd
                    SqlCommand cmd = new SqlCommand(requete, connexionbd);

                    //Ouverture de la connexion
                    //connexionbd.Open();

                    cmd.ExecuteNonQuery();

                    //Femreture de la connexion
                    connexionbd.Close();

                    //Message de confirmation
                    MessageBox.Show("Le stagiaire a été supprimé avec succès!", "Confrimation de la supression");
                }
            }
            catch (Exception ex)
            {
                connexionbd.Close();
                MessageBox.Show("probleme avac la base donnee", ex.Message, MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            //Vider les champs
            Item2Numero.Clear();
            Item2Nom.Clear();
            Item2Prenom.Clear();
            Item2Birthdate.Text = null;
            Gender.SelectedIndex = -1;
            progComboBox.SelectedIndex = -1;
        }

        /// <summary>
        /// Effacer les données saisie des textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Item2btn3_Click(object sender, RoutedEventArgs e)
        {
            Item2Numero.Clear();
            Item2Nom.Clear();
            Item2Prenom.Clear();
            Item2Birthdate.Text = null;
            Gender.SelectedIndex = -1;
            progComboBox.SelectedIndex = -1;

        }

        /// <summary>
        /// Afficher la liste des stagiares dans ListView qui sont inscrit
        /// dans le meme programme selectionne a partir du combobox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            

            //Requete pour récupérer les numero a partire de la table program
            string marequete = $"select * from stagiaire where programme = '{cb_prog_num.SelectedItem}'";

            //La commande a éxécuter
            SqlCommand cmd = new SqlCommand(marequete, connexionbd);

            connexionbd.Open(); //Ouvrir la connexion
            try {
                SqlDataReader dr = cmd.ExecuteReader();//Lire les donnée dans la table de program

                DataTable dt = new DataTable();
                dt.Load(dr);


                connexionbd.Close(); //Fermer la connexion

                Lststagiaireparprog.ItemsSource = dt.DefaultView;
            }
            catch (Exception ex)
            {
                connexionbd.Close();
                MessageBox.Show(ex.Message, "probleme avac la base donnee", MessageBoxButton.OK, MessageBoxImage.Warning);
            }



        }

    }

}
